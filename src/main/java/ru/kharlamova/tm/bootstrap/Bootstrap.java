package ru.kharlamova.tm.bootstrap;

import ru.kharlamova.tm.api.ICommandController;
import ru.kharlamova.tm.api.ICommandRepository;
import ru.kharlamova.tm.api.ICommandService;
import ru.kharlamova.tm.constant.ArgumentConst;
import ru.kharlamova.tm.constant.TerminalConst;
import ru.kharlamova.tm.controller.CommandController;
import ru.kharlamova.tm.repository.CommandRepository;
import ru.kharlamova.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService= new CommandService(commandRepository);

    private final ICommandController commandController= new CommandController(commandService);

    public void run(final String... args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public void parseCommand(String arg) {
        if (arg == null) return;
        switch (arg) {
            case TerminalConst.CMD_ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.CMD_HELP:
                commandController.showHelp();
                break;
            case TerminalConst.CMD_INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.CMD_EXIT:
                commandController.exit();
                break;
            default:
                showIncorrectCommand();
        }
    }

    public void parseArg(String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ARG_HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.showSystemInfo();
                break;
            default:
                showIncorrectArgument();
        }
    }

    public void showIncorrectCommand() {
        System.out.println("Error! Command not found...");
    }

    public void showIncorrectArgument() {
        System.out.println("Error! Argument not found...");
    }

    public boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

}
